trigger OpportunityCustomFieldCheck on Opportunity (after update) {
     OpportunityCustomFieldCheck_helper.checkForLineItem(Trigger.newMap.keySet());
}