trigger AccountTeamMembers_Trigger on Tech_light__Account_Team_Members__c (after insert) {

    //Run Condition after Insert
    if(Trigger.isafter && Trigger.isInsert){
    	AccountTeamMembers_TriggerHandler.afterInsert(Trigger.new);
    }

    //Run Condition after Update
    if(Trigger.isafter && Trigger.isUpdate){
    	AccountTeamMembers_TriggerHandler.afterUpdate(Trigger.new,Trigger.oldMap);
    }
}