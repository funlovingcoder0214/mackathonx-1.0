trigger ClosedOpportunityTrigger on Opportunity (after insert, before update) {
      List<Opportunity> oppToUpdate = [SELECT Id,Name FROM Opportunity WHERE StageName LIKE 'Closed Won' AND Id IN :Trigger.newMap.keySet()];
   LIST<Task> taskToUpdate = new LIST<Task>();
    for(Opportunity o : oppToUpdate)
{
    Task t = new Task(Subject='Follow Up Test Task',WhatId = o.ID);
    taskToUpdate.add(t);
}
    if(taskToUpdate.size()>0)
    {
    insert taskToUpdate;
    System.debug('Success');
    }
}