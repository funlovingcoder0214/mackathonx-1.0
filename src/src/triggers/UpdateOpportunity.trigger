trigger UpdateOpportunity on Opportunity (before update) {
 if (Trigger.isUpdate)
 {  List<Opportunity> OppList = Trigger.new;
     OpportunityUpdate.updateOpp(OppList);
 }
}