trigger EntryTrigger on Entry__c (after insert,after delete,after update,after undelete) {
	EntryTriggerHandler.OldEntries = Trigger.old;	
	EntryTriggerHandler.NewEntries = Trigger.new;
	
	if (Trigger.isInsert && Trigger.isAfter)
		{
			system.debug('trigger');
			EntryTriggerHandler.onAfterInsert(Trigger.new,true,true);
		}
		if (Trigger.isUpdate && Trigger.isAfter)
		{
			EntryTriggerHandler.onAfterUpdate(Trigger.new,true,false);
		}   
		if(Trigger.isDelete && Trigger.isAfter)
		{
			EntryTriggerHandler.onAfterDelete(Trigger.Old,false,true);
		}
		if(Trigger.isUndelete && Trigger.isAfter)
		{
			EntryTriggerHandler.onAfterUnDelete(Trigger.Old,false,false);
		} 
}