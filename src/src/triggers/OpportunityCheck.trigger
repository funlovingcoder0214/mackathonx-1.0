trigger OpportunityCheck on Opportunity (before update) {
for(Opportunity opp : Trigger.new)
{
    if(opp.StageName == 'Closed Won' || opp.StageName == 'Closed Lost')
    {
        opp.CloseDate = date.today(); 
    }
}
}