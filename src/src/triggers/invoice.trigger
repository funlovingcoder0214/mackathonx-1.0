trigger invoice on Invoice__c (before insert) {
    String fieldAPIName1,fieldAPIName2,fieldAPIName3;
    Map<String, String> invoiceMappingMap = new Map<String, String>();
	for(Invoice_Status_Mapping__mdt mapping : [SELECT Field_1__c,Field_2__c ,Field_3__c ,
                                         		  Value_1__c,Value_2__c,Value_3__c,Result_Status__c  
                                           FROM Invoice_Status_Mapping__mdt]) {
    	invoiceMappingMap.put(mapping.Value_1__c+'~'+mapping.Value_2__c+'~'+mapping.Value_3__c,
                              mapping.Result_Status__c);                                           
        fieldAPIName1 = mapping.Field_1__c;
        fieldAPIName2 = mapping.Field_2__c;
        fieldAPIName3 = mapping.Field_3__c;
    }
    
    String mappingKey;
    for(Invoice__c ac : Trigger.new)
    {
        mappingKey = ac.get(fieldAPIName1)+'~'+ac.get(fieldAPIName2)+'~'+ac.get(fieldAPIName3);	
        if(invoiceMappingMap.containsKey(mappingKey)) {
            ac.Status__c  = invoiceMappingMap.get(mappingKey); 
        }
    }
}

//  if(ac.Entry_Status__c  == a.Value_1__c && ac.Close_Status__c  == a.Value_2__c && ac.Approved_Status__c  == a.Value_3__c)