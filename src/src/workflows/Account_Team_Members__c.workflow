<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_To_None_Access</fullName>
        <field>Object_Access__c</field>
        <literalValue>None</literalValue>
        <name>Change To None Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Scheduled_End_Date_Null</fullName>
        <field>Scheduled_End_Date__c</field>
        <name>Scheduled End Date Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Fired_Field</fullName>
        <field>Fired__c</field>
        <literalValue>1</literalValue>
        <name>Update Fired Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fire Scheduled Flow</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account_Team_Members__c.Scheduled_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Team_Members__c.Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Change_To_None_Access</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Scheduled_End_Date_Null</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Fired_Field</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account_Team_Members__c.Scheduled_End_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
