<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Invoice object 1</label>
    <protected>false</protected>
    <values>
        <field>Field_1__c</field>
        <value xsi:type="xsd:string">Entry_Status__c</value>
    </values>
    <values>
        <field>Field_2__c</field>
        <value xsi:type="xsd:string">Close_Status__c</value>
    </values>
    <values>
        <field>Field_3__c</field>
        <value xsi:type="xsd:string">Approved_Status__c</value>
    </values>
    <values>
        <field>Result_Status__c</field>
        <value xsi:type="xsd:string">Submitted to finance</value>
    </values>
    <values>
        <field>Value_1__c</field>
        <value xsi:type="xsd:string">Postable</value>
    </values>
    <values>
        <field>Value_2__c</field>
        <value xsi:type="xsd:string">Open</value>
    </values>
    <values>
        <field>Value_3__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
</CustomMetadata>
