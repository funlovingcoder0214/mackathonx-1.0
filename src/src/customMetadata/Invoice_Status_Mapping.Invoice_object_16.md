<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Invoice_object_16</label>
    <protected>false</protected>
    <values>
        <field>Field_1__c</field>
        <value xsi:type="xsd:string">Tech_light__Entry_Status__c</value>
    </values>
    <values>
        <field>Field_2__c</field>
        <value xsi:type="xsd:string">Tech_light__Close_Status__c</value>
    </values>
    <values>
        <field>Field_3__c</field>
        <value xsi:type="xsd:string">Tech_light__Approved_Status__c</value>
    </values>
    <values>
        <field>Result_Status__c</field>
        <value xsi:type="xsd:string">On-Hold</value>
    </values>
    <values>
        <field>Value_1__c</field>
        <value xsi:type="xsd:string">Recycle</value>
    </values>
    <values>
        <field>Value_2__c</field>
        <value xsi:type="xsd:string">Open</value>
    </values>
    <values>
        <field>Value_3__c</field>
        <value xsi:type="xsd:string">Pending</value>
    </values>
</CustomMetadata>
