public with sharing class OpportunityController {
    public Opportunity opp;
    public List<cProduct2> ProductToShow {get; set;}
    public List<OpportunityLineItemWrapper> selectedProducts {get;set;}
    public Id productToDelete {get;set;}
    public Id opportunityId {get;set;}
    public List<OpportunityLineItem> opportunityLineItemFinalList = new List<OpportunityLineItem>();
    public List<Opportunity> objPricebook2Id{get;set;}
    public static Id opportuntyPriceBook2Id{get;set;}
    
    
    public OpportunityController(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();
        opportunityId = stdController.getId();
        opportuntyPriceBook2Id = [SELECT Pricebook2Id FROM Opportunity WHERE Id =:opportunityId ].Pricebook2Id;
        system.debug('rajat==='+opportuntyPriceBook2Id);
        
    }
    
    
    public List<cProduct2> getProducts()
    {
    if(ProductToShow == null) {
			ProductToShow = new List<cProduct2>();
			for(Product2 p: [SELECT Family,Id,IsActive,Name,ProductCode,(SELECT Id,Pricebook2Id,Product2Id,UnitPrice FROM PricebookEntries where Pricebook2Id =: opportuntyPriceBook2Id ) FROM Product2]) {
				// As each contact is processed we create a new cContact object and add it to the contactList
				ProductToShow.add(new cProduct2(p));
			}
		}
		return ProductToShow;
    }
    
    
    public PageReference processSelected() {
		
		selectedProducts = new List<OpportunityLineItemWrapper>();

			for(cProduct2 cPro: ProductToShow) {
			if(cPro.selected == true) {
				selectedProducts.add(new OpportunityLineItemWrapper(cPro.prod));
			}
		}

		
		return null;
	}
public List<OpportunityLineItemWrapper> getOppLineItem()
{
return selectedProducts;
}
public PageReference removeFromList()
{
	Integer i = 0;
	//List<OpportunityLineItemWrapper> toPerformDelete = selectedProducts ;
	for(OpportunityLineItemWrapper opn : selectedProducts)
	{ 
	if(opn.oplineitem.Id == productToDelete)
		{
		system.debug(productToDelete);
		selectedProducts.remove(i);
		break;
		}
		i++;
	}
	//delete toPerformDelete;
	return null;
}
public PageReference saveLineItems()
{	objPricebook2Id = [SELECT Pricebook2Id FROM Opportunity WHERE Id =: opportunityId]; 
	system.debug(objPricebook2Id);
	for(OpportunityLineItemWrapper w : selectedProducts ){
	OpportunityLineItem oppTemp = new OpportunityLineItem(OpportunityId=opportunityId,PricebookEntryId = w.oplineitem.PriceBookEntries[0].Id, 
									  Quantity = w.quantity,UnitPrice = w.unitPrice);
	opportunityLineItemFinalList.add(oppTemp);
	}
	
	system.debug(opportunityLineItemFinalList);
	insert opportunityLineItemFinalList;
	system.debug(opportunityLineItemFinalList);
	PageReference pg = new PageReference('/'+opportunityId);
	pg.setRedirect(true);
     ApexPages.addmessage(new Apexpages.Message(ApexPages.Severity.Info,'No Record Found' ));
	selectedProducts = new List<OpportunityLineItemWrapper>();
	return pg;
}
public PageReference cancel()
{
PageReference pg = new PageReference('/apex/OpportunityDetailPage?id='+opportunityId);
	pg.setRedirect(true);
	selectedProducts = new List<OpportunityLineItemWrapper>();
	ApexPages.addmessage(new Apexpages.Message(ApexPages.Severity.Info,'No Products were added' ));
	return pg;
}

 class cProduct2 {
		public Product2 prod {get; set;}
		public Boolean selected {get; set;}

			public cProduct2(Product2 c) {
			prod = c;
			selected = false;
		}
	} 
	Class OpportunityLineItemWrapper
	{
	public Product2 oplineitem{get;set;}
	public Integer quantity {get;set;}
	public Decimal unitPrice{get;set;}
	public Decimal totalPrice {get;set;} 
	public OpportunityLineItemWrapper(Product2 oli)
	{ 
		oplineitem = oli;
  		unitPrice = oli.PriceBookEntries[0].UnitPrice;
		quantity = 1;
		totalPrice = quantity*unitPrice;
	}
	}  
	
}