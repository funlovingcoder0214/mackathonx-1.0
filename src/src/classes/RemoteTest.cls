global with sharing class RemoteTest {
	public String accountName { get; set; }
	public static Account acct { get; set; }
	public static List<Contact> con {get;set;} 
	public Account acctName{get;set;}
    public List<SelectOption> getAccountList() { 

       List<SelectOption> options = new List<SelectOption>();

       options.add(new SelectOption('','--Select Account --'));

       for(Account a:[select Id,Name from Account] )
       {
           options.add(new SelectOption(a.id,a.name));   
   }
       return options; 
   }
 @RemoteAction
global static String getAccountContacts(String accountName) 
{
	con = new List<Contact>();
	acct = [Select Id,Name,(select Name,Email,Title from Contacts) from Account where Name  =:accountName];
	con.addAll(acct.Contacts);
	system.debug(con);
	if(con != null && acct.Contacts!= null)
	{
	return System.JSON.serialize(con);
	}
	else{
	return null;
	}
}  
}