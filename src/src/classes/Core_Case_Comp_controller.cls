public class Core_Case_Comp_controller {

    @auraEnabled
    public static List<caseWrapper> caseFetch(){
        List<caseWrapper> caseList = new List<caseWrapper>();
        for(case obj: [Select Id,CaseNumber from case]){
            caseList.add(new caseWrapper(obj,false));
        }
        return caseList;
    }
    
    @auraEnabled
    public static List<caseWrapper> casesDelete(String caseWrapperList){
        List<caseWrapper> casesList = (List<caseWrapper>)json.deserialize(caseWrapperList, List<caseWrapper>.class);
        List<Case> casesToDelete = new List<Case>();
        for(caseWrapper caseWrap: casesList){
            if(caseWrap.IsDelete){
                casesToDelete.add(caseWrap.caseObj);
                 
            }
        }
       
        if(casesToDelete.size() > 0){
            delete casesToDelete;
        }
         return caseFetch();
    }
        
        public class caseWrapper {
            
            @auraEnabled
            public case caseObj;
            @auraEnabled
            public boolean isDelete;
            
            public caseWrapper(case caseObj,boolean isDelete){
                this.caseObj = caseObj;
                this.isDelete = isDelete;
            }
        }
}