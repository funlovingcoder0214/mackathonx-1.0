/*
*   Class Name: AccountTeamMembers_TriggerHandler
*   Description: Handler class for Tech_light__Account_Team_Members__c
*	Author : Rajat Jain 10/26/2018
*	Extra Comments - There is still scope of Coding Standard
*				   - Test Class is missing
*/

public without sharing class AccountTeamMembers_TriggerHandler {

    public static final String READ_ACCESS = 'Read';
    public static final String EDIT_ACCESS = 'Edit';

    public static final String SALES_ROLE = 'Sales';
    public static final String SUPPORT_OWNER_ROLE = 'Support Owner';

    /*
    * Method Name: afterInsert
    * Description: calling this method on 'after insert' trigger event
    * @param: List<Tech_light__Account_Team_Members__c> newList
    * @return: void
    */
    public static void afterInsert(List<Tech_light__Account_Team_Members__c> newList)
    {
    	sharingAccountTeamMember(newList);
    }

    /*
    * Method Name: afterUpdate
    * Description: calling this method on 'after Update' trigger event
    * @param: List<Tech_light__Account_Team_Members__c> newList
    * @param: List<Tech_light__Account_Team_Members__c> OldMap
    * @return: void
    */
    public static void afterUpdate(List<Tech_light__Account_Team_Members__c> newList, map<Id,Tech_light__Account_Team_Members__c> OldMap){
    	sharingAccountTeamMemberOnEdit(newList,OldMap);
    }

	/*
    * Method Name: sharingAccountTeamMember
    * Description: Method to Share record Acces of a Account
    * @param: List<Tech_light__Account_Team_Members__c> newList
    * @return: void
    */
    public static void sharingAccountTeamMember(List<Tech_light__Account_Team_Members__c> newList){

    	list<Tech_light__Account_Team_Members__c> teamMemberRecordToUpdate = new list<Tech_light__Account_Team_Members__c>();
		set<Id> teamMemberRecIdSet = new set<Id>();

    	//Iterate over Account Team Member to Provide Access as needed
    	for(Tech_light__Account_Team_Members__c accntTeamMemRec : newList){

    		if(accntTeamMemRec.Tech_light__Role__c == SALES_ROLE ){
    			shareRecordsWithUser(accntTeamMemRec, READ_ACCESS , READ_ACCESS , EDIT_ACCESS );
    		}else if(accntTeamMemRec.Tech_light__Role__c == SUPPORT_OWNER_ROLE ){
    			shareRecordsWithUser(accntTeamMemRec, READ_ACCESS , EDIT_ACCESS , READ_ACCESS );
    		}else{
    			shareRecordsWithUser(accntTeamMemRec, READ_ACCESS , READ_ACCESS , READ_ACCESS);
    		}
			teamMemberRecIdSet.add(accntTeamMemRec.Id);


			Date dtRec = accntTeamMemRec.Tech_light__End_Date__c;

    		Tech_light__Account_Team_Members__c tempTeamMemberRec =  new Tech_light__Account_Team_Members__c(Id = accntTeamMemRec.Id , Tech_light__Scheduled_End_Date__c = datetime.newInstance(dtRec.year(), dtRec.month(),dtRec.day()));
			teamMemberRecordToUpdate.add(tempTeamMemberRec);

    	}

    	//To Update Scheduled End Date on Team Member Object
    	if(teamMemberRecordToUpdate.size() > 0){
    		update teamMemberRecordToUpdate;
    	}

    	populateShareReordIdss(teamMemberRecIdSet);

    }

    /*
    * Method Name: shareRecordsWithReadAccess
    * Description: Method to provide read access of Opportunity, Case, Contact object to user
    * @param: List<Tech_light__Account_Team_Members__c> newList
    * @param: string accountAccess
    * @param: string caseAccess
    * @param: string oppAccess
    * @return: void
    */
    public static void shareRecordsWithUser(Tech_light__Account_Team_Members__c accntTeamMemRec, string accountAccess, string caseAccess, string oppAccess){
    		AccountShare accRec = new AccountShare(AccountId = accntTeamMemRec.Tech_light__Account__c , UserOrGroupId = accntTeamMemRec.Tech_light__User__c , AccountAccessLevel = accountAccess ,CaseAccessLevel = caseAccess , OpportunityAccessLevel = oppAccess);
    		insert accRec;
    }


    /*
    * Method Name: populateShareReordIdss
    * Description: Method to populate Share Object Id on Team Member Record
    * @param: Set<Id> teamMemberIds
    * @return void
    */
    @future
    public static void populateShareReordIdss(Set<Id> teamMemberIds) {

    	//Function Properties
    	set<Id> userIdSet = new set<Id>();
    	Map<ID,CaseShare> mapOfCaseShare = new Map<ID, CaseShare>();
    	Map<ID,AccountShare> mapOfAccountShare = new Map<ID, AccountShare>();
    	Map<ID,OpportunityShare> mapOfOpportunityShare = new Map<ID, OpportunityShare>();
    	list<Tech_light__Account_Team_Members__c> teamMemberList = new list<Tech_light__Account_Team_Members__c>();
    	list<Tech_light__Account_Team_Members__c> teamMemberListToUpdate = new list<Tech_light__Account_Team_Members__c>();

    	//Loop to get Account Team Member Details
    	for(Tech_light__Account_Team_Members__c accntTeamMemRec : [SELECT ID,Tech_light__Account__c,Tech_light__Case_Share__c,Tech_light__Contact_Share__c,
    																		Tech_light__End_Date__c,Tech_light__Fired__c,Tech_light__Object_Access__c,Tech_light__Opportunity_Share__c,
    																		Tech_light__Role__c,Tech_light__Scheduled_End_Date__c,Tech_light__Start_Date__c,Tech_light__User__c
    																	FROM Tech_light__Account_Team_Members__c
    																	WHERE ID IN :teamMemberIds]){
			userIdSet.add(accntTeamMemRec.Tech_light__User__c);
			teamMemberList.add(accntTeamMemRec);
    	}

    	//Loop to get CaseShare Record
    	for(CaseShare caseShareObj : [Select Id, UserOrGroupId from CaseShare WHERE UserOrGroupId IN :userIdSet]){
		   mapOfCaseShare.put(caseShareObj.UserOrGroupId, caseShareObj);
		}

		//Loop to get AccountShare Record
		for(AccountShare AccountShareObj : [Select Id, UserOrGroupId from AccountShare WHERE UserOrGroupId IN :userIdSet]){
		   mapOfAccountShare.put(AccountShareObj.UserOrGroupId, AccountShareObj);
		}

		//Loop to get OpportunityShare Record
		for(OpportunityShare oppShareObj : [Select Id, UserOrGroupId from OpportunityShare where UserOrGroupId IN :userIdSet]){
		   mapOfOpportunityShare.put(oppShareObj.UserOrGroupId, oppShareObj);
		}

		//Loop to get Share ID for Team Members
		boolean addRecordToList;
		for(Tech_light__Account_Team_Members__c accntTeamMemRec : teamMemberList ){

				addRecordToList = false;

				if(mapOfCaseShare.containsKey(accntTeamMemRec.Tech_light__User__c) ){
					accntTeamMemRec.Tech_light__Case_Share__c = mapOfCaseShare.get(accntTeamMemRec.Tech_light__User__c).Id;
					addRecordToList = true;
				}

				if(mapOfAccountShare.containsKey(accntTeamMemRec.Tech_light__User__c) ){
					accntTeamMemRec.Tech_light__Contact_Share__c = mapOfAccountShare.get(accntTeamMemRec.Tech_light__User__c).Id;
					addRecordToList = true;
				}

				if(mapOfOpportunityShare.containsKey(accntTeamMemRec.Tech_light__User__c) ){
					accntTeamMemRec.Tech_light__Opportunity_Share__c = mapOfOpportunityShare.get(accntTeamMemRec.Tech_light__User__c).Id;
					addRecordToList = true;
				}

				if(addRecordToList){
					teamMemberListToUpdate.add(accntTeamMemRec);
				}
		}

		//Update Operation
		if(teamMemberListToUpdate.size() > 0){
			update teamMemberListToUpdate;
		}
    }
	/*
    * Method Name: sharingAccountTeamMemberOnEdit
    * Description: Method to Share record Acces of a Account
    * @param: List<Tech_light__Account_Team_Members__c> newList
    * @param: List<Tech_light__Account_Team_Members__c> OldMap
    * @return: void
    */
    public static void sharingAccountTeamMemberOnEdit(List<Tech_light__Account_Team_Members__c> newList, map<Id,Tech_light__Account_Team_Members__c> OldMap){
    	list<Tech_light__Account_Team_Members__c> newTeamMemberList = new list<Tech_light__Account_Team_Members__c>();
    	for(Tech_light__Account_Team_Members__c accntTeamMemRec : newList){
    		if(accntTeamMemRec.Tech_light__Role__c != OldMap.get(accntTeamMemRec.Id).Tech_light__Role__c
    				|| accntTeamMemRec.Tech_light__End_Date__c != OldMap.get(accntTeamMemRec.Id).Tech_light__End_Date__c ){
    			newTeamMemberList.add(accntTeamMemRec);
    		}
    	}

    	if(newTeamMemberList.size() > 0){
    		sharingAccountTeamMember(newTeamMemberList);
    	}
    }
}