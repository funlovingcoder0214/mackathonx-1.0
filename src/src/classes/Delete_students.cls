public with sharing class Delete_students {
 public List<Student__c> stu_List {get;set;} 
private ApexPages.StandardSetController standardController;  
private Set<Id> stuIds = new Set<Id>();

    public Delete_students(ApexPages.StandardSetController standardController){
        this.standardController = standardController;
        stu_List = new List<Student__c>();
        for (Student__c stu : (List<Student__c>)standardController.getSelected()){ 
            stuIds.add(stu.Id);
        }
        stu_List = [SELECT FirstName__c,Last_Name__c, Age__c,DOB__c,Class__c FROM Student__c WHERE ID IN: stuIds];
        System.debug(stu_List);
    }
    public PageReference deleteStudents()
    {
    	delete stu_List;
    	PageReference pre = new PageReference('https://saletech-dev-ed.my.salesforce.com/a0Q?fcf=00B28000008GqGy');
    	pre.setRedirect(true);
        return pre;
    }
    public PageReference cancel()
    {
    	PageReference pre = new PageReference('https://saletech-dev-ed.my.salesforce.com/a0Q?fcf=00B28000008GqGy');
    	pre.setRedirect(true);
        return pre;
    }
}