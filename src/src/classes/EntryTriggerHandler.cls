public without sharing class EntryTriggerHandler {
	public static list<Entry__c> OldEntries = new list<Entry__c>();
	public static list<Entry__c> NewEntries = new list<Entry__c>();
	
	public static void onAfterInsert(list<Entry__c> listOfEntry,boolean flag1,boolean flag2)
	{
		system.debug('function');
		updateTotalEntryField(listOfEntry,flag1,flag2);
	} 
	public static void onAfterUpdate(list<Entry__c> listOfEntry,boolean flag1,boolean flag2)
	{
		updateTotalEntryField(listOfEntry,flag1,flag2);
		updateTotalEntryField(OldEntries,false,true);
	} 
	public static void onAfterDelete(list<Entry__c> ListOfEntry,boolean flag1,boolean flag2)
	{
		updateTotalEntryField(listOfEntry,flag1,flag2);	
	}
	public static void onAfterUnDelete(list<Entry__c> ListOfEntry,boolean flag1,boolean flag2)
	{
		updateTotalEntryField(listOfEntry,flag1,flag2);
	}
	public static void updateTotalEntryField(list<Entry__c> listOfEntry,boolean flag1,boolean flag2)
	{
		Map< ID ,List<Entry__c>> recordsToBeInserted = new Map<ID,List<Entry__c>>();
		List<Entry__c> tempEntryList; 
		Set<Id> tournamentIds = new Set<Id>();
		Set<Id> RegistrationIds = new Set<Id>();
		Set<Id> oldTournamentIds = new Set<Id>();
		Set<Id> oldRegistrationIds = new Set<Id>();
		/*
		Entry__c --- Tournament__c, Registration__c
		
		RegIds -- > ALL Entries 0
		
		Set<Id> setOfTournamentIds --->>>> Map<Id, Tournament__c> // 2
		
		Reg--> {E1-T1, E2-T2, E3-T3}
		
		
		*/
		system.debug(OldEntries);
		system.debug(NewEntries);
		
		//Creating A Map Of Registration And Its List Of Its Entries
		for(Entry__c eRec : listOfEntry)
		{	tempEntryList = new List<Entry__c>();
			tournamentIds.add(eRec.Tournament__c);
			RegistrationIds.add(eRec.Registration__c);
			if(recordsToBeInserted.containskey(eRec.Registration__c))
			{
				tempEntryList = recordsToBeInserted.get(eRec.Registration__c);
				tempEntryList.add(eRec);
				recordsToBeInserted.put(eRec.Registration__c,tempEntryList);
			}
			else
			{	
				tempEntryList.add(eRec);
				recordsToBeInserted.put(eRec.Registration__c,tempEntryList);
			}
			
		}
		
		
	/*	if(flag1 == true && flag2 == false)
		{
			for(Id recId : OldMapEntry.keyset())
			{
				oldTournamentIds.add(OldMapEntry.get(recId).Tournament__c);
				oldRegistrationIds.add(OldMapEntry.get(recId).Registration__c);
			}		
			system.debug(oldTournamentIds);
			system.debug(oldRegistrationIds);
		}
		*/
		
		//Map To Store Registration Records And There old Total_Entry_Fee
		Map<Id,Decimal> registrationTempMap = new map<Id,Decimal>();
		for(Registration__c rec : [SELECT Entry_Fee_Total__c,Id,Name,Player__c,Registration_Date__c FROM Registration__c WHERE ID In :RegistrationIds])
		{
			registrationTempMap.put(rec.Id,rec.Entry_Fee_Total__c);
		}
		
		
		// Map To Store Registration Ids And List Of All Entries Against That Registration
		Map<Id,List<Entry__c>> registrationToEntryMap = new Map<Id,List<Entry__c>>();
		List<Entry__c> tempList = new List<Entry__c>();
		system.debug(RegistrationIds);
		for(Entry__c entryRec : [SELECT Id,Registration__c,Registration__r.Player__c,Tournament__r.Game__c FROM Entry__c WHERE Registration__c IN :RegistrationIds])
		{
			system.debug(entryRec);
			tempList = new List<Entry__c>();
			if(registrationToEntryMap.containskey(entryRec.Registration__c))
			{	
				tempList = registrationToEntryMap.get(entryRec.Registration__c);
				tempList.add(entryRec);
				registrationToEntryMap.put(entryRec.Registration__c,tempList);
			}
			else
			{	
				tempList.add(entryRec);
				registrationToEntryMap.put(entryRec.Registration__c,tempList);
			}
		}
		
		Map<Id,Map<String,Integer>> gameCounterMap = new Map<Id,Map<String,Integer>>();
		Map<String,Integer> tempMap = new Map<String,Integer>();
		Integer tempCounter;
		Integer finalCounter;
		Id userId;
		String favGame;
		list<Contact> contactToUpdate = new list<Contact>();
		for(Id regId : registrationToEntryMap.keyset())
		{	
			finalCounter = 1;
			
			for(Entry__c entryRec : registrationToEntryMap.get(regId))
			{	tempMap = new Map<String,Integer>();
				tempCounter = 0;
				userId = entryRec.Registration__r.Player__c;
				if(gameCounterMap.containsKey(regId))
				{
					if((gameCounterMap.get(regId)).containsKey(entryRec.Tournament__r.Game__c))
					{
						tempMap = gameCounterMap.get(regId);
						tempCounter = (tempMap.get(entryRec.Tournament__r.Game__c)) + 1;
						tempMap.put(entryRec.Tournament__r.Game__c,tempCounter);
						gameCounterMap.put(regId,tempMap);	
						if(tempCounter >= finalCounter)
						{
							favGame = entryRec.Tournament__r.Game__c;
						}
					}
					else
					{
						tempMap = gameCounterMap.get(regId);
						tempMap.put(entryRec.Tournament__r.Game__c,1);
						gameCounterMap.put(regId,tempMap);
					}	
				}
				else
				{
						tempMap.put(entryRec.Tournament__r.Game__c,1);
						gameCounterMap.put(regId,tempMap);
						favGame = entryRec.Tournament__r.Game__c;
				}
		}
			Contact contactTemp = new Contact(Id = userId,Favorite_Game__c = favGame  );
			contactToUpdate.add(contactTemp);
		}
		update contactToUpdate;
		system.debug(gameCounterMap);
		
		//Map To Store Tournament And Its Entry Fee
		Map<ID,Tournament__c> tournamnet = new Map<ID,Tournament__c>();
		for(Tournament__c trmntRec : [SELECT Entry_Fee__c,Game__c,Id,Name FROM Tournament__c where Id In :tournamentIds]) // 30000 --- 2
		{
			tournamnet.put(trmntRec.ID,trmntRec);
		}
		
		// Updating Total_Entry_Fee_field Value
		List<Registration__c> RegistrationRecToUpdate = new List<Registration__c>(); 
		for(Id a :recordsToBeInserted.keyset())
		{	Decimal oldEntryValue = registrationTempMap.get(a);
			Registration__c temp = new Registration__c();
			Decimal totalEntryFee = 0;
			for(Entry__c e : recordsToBeInserted.get(a))
			{
				totalEntryFee += (tournamnet.get(e.Tournament__c)).Entry_Fee__c;
				
			}
			temp.ID = a;
			if((flag1 == true && flag2 == true) || (flag1 == true && flag2 == false) )
			{
				if(oldEntryValue >0)
				{
					temp.Entry_Fee_Total__c = oldEntryValue + totalEntryFee;
				}
				else
				{
					temp.Entry_Fee_Total__c = totalEntryFee;
				}
			}
			else if(flag1 == false && flag2 == true)
			{
				temp.Entry_Fee_Total__c = oldEntryValue - totalEntryFee;
			}
			RegistrationRecToUpdate.add(temp);
		}
		update RegistrationRecToUpdate;
		
		
	} 
}