public class OpportunityUpdate {

    public static void updateOpp(List<Opportunity> Temp_List)
    { //List<Account> ListAccount = [SELECT Account.Name FROM Opportunity];
        for(Opportunity opp : Temp_List)
        {
            if(String.isNotBlank(opp.BillToContact__c ) && String.isBlank(opp.Manager__c ))
            {
              opp.Manager__c =  opp.AccountId; 
            }
        }
    }
}