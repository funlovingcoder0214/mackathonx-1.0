public with sharing class recordToDisplay {
 public string queryString{get;set;}   
 public String subString;
 public List<sObject> sobjList{get;set;}
 public List<String> objectFields{get;set;}
 public List<String> label{get;set;}
 public PageReference fetchRecords()
 {
 	
 	if(queryString!= null && queryString!='')
 	{
 		sobjList = new List<sObject>();
 		try{
 		sobjList = Database.query(queryString);
 		system.debug(sobjList);
 		system.debug(queryString);
 		subString=queryString.substring(queryString.IndexOf('T')+2,queryString.IndexOf('FROM'));
 		system.debug(subString);
 		objectFields=subString.split(',');//splits the string based on whitespace  
		//using java foreach loop to print elements of string array  
			for(String w:objectFields)
			{  

				system.debug(w);
			
			}  
 		}
 		catch(System.QueryException q)
		{
  			System.debug(q);
		}
 	}
 	System.debug(sobjList);
 	return null;
 }
}