public with sharing class MapSetId {
	public static Map<Class__c,List<Student__c>> ClassStudentMap = new Map<Class__c,List<Student__c>>();
  public static Map<Class__c,List<Student__c>> setToReturn()
  {
  	
  for (Class__c clRec : [SELECT Id,Name,(Select Id from Students__r) FROM Class__c]){
  		ClassStudentMap.put(clRec,clRec.Students__r);
  }
  return ClassStudentMap;
  }  
}