public with sharing class PdfGeneratorController {
private final Account Acct;
    public ID parentId {get;set;}
  public String pdfName {get;set;}

    public PdfGeneratorController(ApexPages.StandardController stdController)
    {
         this.acct = (Account)stdController.getRecord();
        ParentId = acct.Id;
        pdfName = 'Account Detail';
           }
  public PageReference savePdf() {

    PageReference pdf = Page.PdfGeneratorTemplate;
    // add parent id to the parameters for standardcontroller
      pdf.getParameters().put('id',parentId);

    // create the new attachment
    Attachment attach = new Attachment();

    // the contents of the attachment from the pdf
    Blob body;

    try {

        // returns the output of the page as a PDF
        body = pdf.getContent();

    // need to pass unit test -- current bug    
    } catch (VisualforceException e) {
       System.assert(false,e.getMessage());
        body = Blob.valueOf('Some Text');
    }

    attach.Body = body;
    // add the user entered name
    attach.Name = pdfName;
    attach.IsPrivate = false;
    // attach the pdf to the account
    attach.ParentId = parentId;
    insert attach;
List<Attachment> attch = [SELECT Id,Name,ParentId,CreatedDate FROM Attachment WHERE ParentId =: parentId AND Name =: pdfName order by CreatedDate];
if(attch.size()>1)
{
	delete attch.get(0);
/*if((attch.get(0)).CreatedDate > (attch.get(1)).CreatedDate)
{
	delete attch.get(1);
}
else
{
	delete attch.get(0);
}*/
}
    // send the user to the account to view results
    return new PageReference('/'+parentId);

  }

}