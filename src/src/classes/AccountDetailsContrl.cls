public class AccountDetailsContrl {
	
    @Auraenabled
    public static list<Account> fetchAccountDetails(){
        list<Account> accountData = [SELECT ID,Name FROM Account LIMIT 5];
        return accountData;
    }
}