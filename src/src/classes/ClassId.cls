public class ClassId {
//public List<Student__c> ListStudent {get;set;}
public static List<Student__c> getListsStudent(String name)
{
    List<Student__c> ListStudent;
    ListStudent = [SELECT Class__c,Name FROM Student__c WHERE Class__r.Name =:name];
    system.debug(ListStudent);
    return ListStudent;
}
}