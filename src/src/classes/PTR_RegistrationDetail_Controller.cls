public without sharing class PTR_RegistrationDetail_Controller {
   public Registration__c registrationRec {get;set;} 
   public String tourGameType {get;set;}
   public List<TrornamentWrapper> tournamentWrapperList {get;set;} 
   public Id registraionId{get;set;}
   public Map<string,Entry__c> entryMap{get;set;}
   public string query;
   public set<Id> tourId {get;set;}
   
   
   // Standard Set Controller for Pagination
   public ApexPages.StandardSetController setCon {get;set;}
   
   
   //Constructor
    public PTR_RegistrationDetail_Controller(ApexPages.StandardController stdController)
    {
    		entryMap = new Map<string,Entry__c>();
    		tourId = new set<Id>() ;
    		registrationRec = (Registration__c)stdController.getRecord();
    		registraionId = stdController.getId();
    		query = 'SELECT Date__c,Entry_Fee__c,Game__c,Id,Name FROM Tournament__c ORDER BY Date__c,Entry_Fee__c';
    		if(registraionId != null)
    		{
    			for(Entry__c eRec:[SELECT Id,Registration__c,Tournament__r.Name,Tournament__c FROM Entry__c WHERE Registration__c =: registraionId ])
    			{
    				entryMap.put(eRec.Tournament__r.Name,eRec);
    				tourId.add(eRec.Tournament__c);
    			}
    		}
    		fetchTournamentList(setConMethod());
    } 
    
    //Method To Generate Game Type SelectLsit
  	public List<SelectOption> getGameType()
	{
		system.debug(registraionId);
  		List<SelectOption> options = new List<SelectOption>();    
  		options.add(new SelectOption('','--Any Type--'));
 		Schema.DescribeFieldResult fieldResult = Tournament__c.Game__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();    
   		for( Schema.PicklistEntry f : ple)
   		{
      		options.add(new SelectOption(f.getLabel(), f.getValue()));
      	}       
  		return options;
	}
    
    //Generating Wrapper Class Of Tournament
   	public List<TrornamentWrapper> fetchTournamentList(ApexPages.StandardSetController setCon)
    {
			tournamentWrapperList = new List<TrornamentWrapper>();
			system.debug('----'+setCon.getRecords());
			for(Tournament__c tourRec: (List<Tournament__c >)setCon.getRecords()) 
			{
				if(entryMap.containsKey(tourRec.Name))
				{
					tournamentWrapperList.add(new TrornamentWrapper(tourRec,true));
				}
				else
				{
					tournamentWrapperList.add(new TrornamentWrapper(tourRec,false));
				}
			}
			return tournamentWrapperList;
    }
    
     
    // method to get Tournament List Using filter
    public void fetchTournamentTypeList()
    {
    	if(tourGameType != null && tourGameType!= '' )
    	{
    		query = 'SELECT Date__c,Entry_Fee__c,Game__c,Id,Name FROM Tournament__c WHERE Game__c = \''+ tourGameType +'\' ORDER BY Date__c,Entry_Fee__c';
    	}
    	else
    	{
    		query = 'SELECT Date__c,Entry_Fee__c,Game__c,Id,Name FROM Tournament__c ORDER BY Date__c,Entry_Fee__c';	
    	}
    		system.debug(query);
    		setCon = null;
    		fetchTournamentList(setConMethod());
    		system.debug('-----'+tourId);
    }
    
    //Method To Set Pagination 
    public ApexPages.StandardSetController setConMethod()
    {
    	if(setCon == null) {                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            			setCon.setpagesize(10);
           }   
            	return setCon;
    
    }
    
    //Method To Save Data
    public PageReference saveData()
    {
    	List<Entry__c> entryListToInsert = new List<Entry__c>();
    	List<Entry__c>	entryListToDelete = new List<Entry__c>();
    	upsert registrationRec;
    	for(TrornamentWrapper wrapperRec : tournamentWrapperList)
    	{
    		
    		if(wrapperRec.selected == true && !tourId.contains(wrapperRec.tour.Id))
    		{
    		Entry__c entryTemp = new Entry__c(Registration__c = registrationRec.Id ,Tournament__c = wrapperRec.tour.Id);
    		entryListToInsert.add(entryTemp);
    		}
    		if(wrapperRec.selected == false && tourId.contains(wrapperRec.tour.Id) )
    		{
    			entryListToDelete.add(entryMap.get(wrapperRec.tour.Name));
    		}
    	}
    	insert entryListToInsert;
   	 	delete entryListToDelete;
   	 	PageReference registrationPage = new PageReference('/' + registrationRec.Id);
        registrationPage.setRedirect(true);
        return registrationPage;
    }
   
    
    //Wrapper Class
    class TrornamentWrapper
     {
		public Tournament__c tour {get; set;}
		public Boolean selected {get; set;}
		
		
		public TrornamentWrapper(Tournament__c c,Boolean flag)
		 {
		 		tour = c;	
		 		selected = flag;
		}
	} 
    
}