public class accountsWithOpptysCon {
        public List<AccountTotal> getaccounttotals() {
        List<AccountTotal> accounttotals = new List<AccountTotal>();
        for(Account a:[select name, (select name,amount from opportunities)
                 from account
                 where id in (select accountid from opportunity)]) {
             accounttotals.add(new AccountTotal(a));
        }
        return accounttotals;
    }
        public class accountTotal {
        public Account account { get; private set; }
        public Opportunity total { get; private set; }
                public accountTotal(Account a) {
            account = a;
            total = new Opportunity(amount = 0);
            for(Opportunity o:a.opportunities) {
                if(o.amount != null) 
                {
                	total.amount += o.amount;
        }
    }
            }
        }
}