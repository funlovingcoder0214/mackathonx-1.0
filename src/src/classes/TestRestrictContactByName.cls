@isTest
public class TestRestrictContactByName {

    @isTest static void testLastNameOfContact()
    {
    Contact con = new Contact(LastName='INVALIDNAME');
    insert con;
        Test.startTest();
       Database.SaveResult result = Database.insert(con, false);
        Test.stopTest();
         System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('Cannot add a contact with lastName as INVALIDNAME',
                             result.getErrors()[0].getMessage());
    }
}