public with sharing class ContactIdList {
    public static void ListOfContact()
    {
    	String ListOfId;
    	List<Account> accountList = new List<Account>();
    	for(Account acct : [SELECT Id,Name,ListOfContact__c,(Select Id from Contacts) FROM Account])
    	{ 		ListOfId='';
    		for(Contact con : acct.Contacts )
    		{
    			if(String.ISBLANK(acct.ListOfContact__c))
    			{
    				ListOfId+= con.Id;	
    			}
    			else
    			{
    			ListOfId+=',' + con.Id;
    			}
  
    		}
    		system.debug(ListOfId);
    		acct.ListOfContact__c = ListOfId;
    		accountList.add(acct);
    	}
    update accountList;
    }
    
}