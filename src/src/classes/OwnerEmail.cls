global class OwnerEmail implements Schedulable {

global void execute(SchedulableContext ctx) {
List<Messaging.SingleEmailMessage> MailToSend = new List<Messaging.SingleEmailMessage>();
for(Opportunity opp :[SELECT LastModifiedDate,Name,OwnerId,Owner.Name,Owner.Email FROM Opportunity WHERE LastModifiedDate <= LAST_N_DAYS:30 limit 10])
{
	
List <String> toAddresses = new List<String>();
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
toAddresses.add(opp.Owner.Email);
mail.setToAddresses(toAddresses);
//mail.setReplyTo('email@domain.com');
mail.setSenderDisplayName('Salesforce Support');
mail.setSubject('Unmodidfied Opportunity');
//mail.setBccSender(false);
//mail.setUseSignature(false);
mail.setPlainTextBody('Your Opportunity '+ opp.Name +'is not Modfied from Last 30 days');
MailToSend.add(mail);

}
Messaging.sendEmail(  MailToSend );   
   }
 
}