<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Battle_Station__c</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Battle Station Construction</label>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>Battle_Station__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Supply__c</tab>
    <tab>Resource__c</tab>
    <tab>Class__c</tab>
    <tab>Student__c</tab>
    <tab>Registration__c</tab>
    <tab>Tournament__c</tab>
    <tab>Entry__c</tab>
</CustomApplication>
