({
    init : function(component, event, helper) {
        var action = component.get("c.fetchAccountDetails");
        action.setCallback(this, function(response) {
            //console.log('**', JSON.stringify(response.getReturnValue()));
            component.set("v.AccountList",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    sendAccountId : function(component, event, helper) {
        console.log('Inside Function');
        var appEvent = $A.get('e.c:BridgeAccountId');
        console.log('**'+ event.currentTarget.getAttribute('data-index'));
        appEvent.setParams({
            'accountId' : event.currentTarget.getAttribute('data-index')
        });
        appEvent.fire();
    }
    
 })