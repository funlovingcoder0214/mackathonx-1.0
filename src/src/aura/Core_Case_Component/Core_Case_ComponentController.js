({
	myAction : function(component, event, helper) {
		
        var action = component.get("c.caseFetch");
        //action.setParams({ "userName" : userName });
        action.setCallback(this,function(responseGroup){
            console.log('user'+JSON.stringify(responseGroup.getReturnValue()));
            var allCase = responseGroup.getReturnValue();
            component.set("v.caseListAll",allCase);
            
            var listRec = [];
            for(var cc =0;cc<10;cc++){
                listRec.push(allCase[cc]);
            }
            component.set("v.caseList",listRec);
            console.log(listRec.length);
            
        });
        
        $A.enqueueAction(action);
        
        
        
	},
    scriptsLoaded : function(component,event,helper){
    	console.log('script loaded');
        $('#sampleTable').DataTable({
               "ordering": true,
            
            
                });
    },
    checkDelete : function(component,event,helper){
        console.log(event.getSource().get("v.label"));
        
        var index = event.getSource().get("v.label");
		console.log(index);
        var caseList = component.get("v.caseListAll");
        
        if(caseList[index].isDelete){
            caseList[index].isDelete = false;
        }
        else{
            caseList[index].isDelete = true;
        }
        
        console.log(JSON.stringify(component.get("v.caseList")));
    },
    
    nextBtn : function(component,event,helper){
        console.log('start');
        var allCase = component.get("v.caseListAll");
        var listRec = [];
        
        var startRecord = component.get("v.startRecord");
        var totalRec = allCase.length;
        var incrementRecs ;
        if(startRecord+20 <= totalRec){
            incrementRecs = startRecord+20;
        }
        else{
            incrementRecs = totalRec;
        }
        var startRec = startRecord+10;
        
        for(var i =startRec;i<incrementRecs;i++){
            console.log('for');
            listRec.push(allCase[i]);
        }
        component.set("v.startRecord",startRecord+10);
        var pageNumber = Math.ceil(incrementRecs/10);
        component.set("v.pageNumber",pageNumber);
        component.set("v.caseList",listRec);
    },
    
    previousBtn : function(component,event,helper){
        
    },
    
     openModal : function(component,event,helper){
       component.set("v.showModal",true);
    },
    closeModel : function(component){
        component.set("v.showModal",false);
        
        
    },
    
    deleteCases : function(component,event,helper){
        
        var action = component.get("c.casesDelete");
        action.setParams({ "caseWrapperList" : JSON.stringify(component.get("v.caseListAll"))});
        action.setCallback(this,function(responseGroup){
            console.log('user'+JSON.stringify(responseGroup.getReturnValue()));
            //var allCase = responseGroup.getReturnValue();
            //component.set("v.caseListAll",allCase);
           // var sizes = component.get("v.caseListAll").length;
            //console.log(sizes);
            $('#sampleTable').DataTable().draw('page');
            
        });
        
        $A.enqueueAction(action);
        
    },
})